/**
* JSV 1.0
* Author: William Rivera
* Date 9/27/11
* 
* JSV aka Javascript Structure Validator will check you javascript structure and verify its format
*/
(function(){	
	function JSV()
	{
		this.results = 'not started';
		this.errors = false;
		this.success = false;
		this.searchString = [];
	}
	window.JSV={
		
		validate:function(props)
		{
			var jsvObj= new JSV();
			error.found=0;
			error.errors=[];
			
			jsvObj.results = 'Loading ... ';
			validator.validate(props.validate || emptyObject, typeOf(props.validate), " for param.validate, you must pass in a string formatted Data");
			validator.validate(props.data || emptyObject, typeOf(props.data), " for param.data, you must pass in a json object");
			if(error.found > 0)
			{
				var msg="You did not pass in the correctly formated parameters to begin validation";
				msg+= "\n The following errors occured: \n";
				for(var i=0,l=error.errors.length; i<l; i++)
					msg += error.errors[i].msg;
					
				jsvObj.errors = msg;
				jsvObj.results = "unable to begin parse, check errors";
				props.callback();
				return;
			}
			//no errors found assign function and begin parse;
			jsvObj.results = 'started parsing';
			jsvObj.searchString=props.validate;
			
			if(typeOf(props.validate) != "Array") props.validate = [props.validate];
			// multisearch
			for(var k in props.validate)
				parse(props.validate[k],props.data,k);
				
			if(error.found > 0)
			{
				var msg="error on fomat  \n";
				for(var i=0,l=error.errors.length; i<l; i++)
					msg += error.errors[i].msg;
					
				jsvObj.errors = msg;
				jsvObj.results = "parse complete erros detected, check errors";
			}else{
				jsvObj.results = "parse complete no errors";
				jsvObj.success=true;
			}
			if(props.callback && (typeOf(props.callback) == "Function"))
				props.callback.call(jsvObj);
			
			return jsvObj;
		}
	}
	
	function parse(on, data, k)
	{
		//get all children remove white space if any
		var args=on.split(/(\{|\(|\[|\#|\$)/); 
		var parent= new parentObj();
		for(var i=0,l=args.length; i<l; i++)
		{
			if(args[i] == '')continue;
			if(args[i+1].match(/\w/g))
			{
				args[i]+=args[i+1];
				args[i+1]='';
			}
			parent.push(args[i]); 
		}
		return parent.parse(k,data,0,0);
	}
	function parentObj(value)
	{
		this.val=  value || null;
		this.child= null;
		this.parent = null;
		this.push = function(val)
		{
			if(this.val==null)
			{
				this.val = val;
			}else{
				if(this.child == null) 
				{
					var obj = new parentObj(val);
					obj.parent = this;
					this.child = obj;
				}else{
					this.child.push(val);
				}
			}
		}
		this.parse = function(searchcnt, data, nesting,loopcount)
		{
			var newNesting=nesting+1;
			var msg = ' for search index '+searchcnt+' within nesting at level:'+nesting+"\n";
			msg+="loop at :"+loopcount+" data as:"+data+"\n";
			var named =(this.val.length>1) ? this.val.split(/(\{|\(|\[|\#|\$)/g) : [] ;
			var val = named[1] || this.val;
			switch(val)
			{
				case "{": validator.validate(data, 'Object',msg);
						break;
				case "[": validator.validate(data, 'Array',msg);
						break;
				case "(": validator.validate(data, 'Function',msg);
						break;
				case "#": validator.validate(data, 'Number',msg);
						break;
				case "$": validator.validate(data, 'String',msg);
						break;
			} 	
			//check if children are named
			if(named[1])
			{
				var childprp = named[2].split(",");
				for(var i=0, l =childprp.length; i<l; i++)
				{
					validator.nameValid(childprp[i],data,msg);
					//follow named path
					if(this.child != null && data[childprp[i]])
						this.child.parse(searchcnt,data[childprp[i]],newNesting,nesting+"."+loopcount);
				}
				return;
			}
			
			if(error.found>0) return;
		
			//get children object for parsing
			var loopcnt=0;
			for(var k in data)
			{
				if(this.child != null && data[k])
					this.child.parse(searchcnt,data[k],newNesting,loopcnt++);
			}
			
		}
	}
	var validator=
	{
		validate:function (param, type, msg)
		{
			if(typeOf(param) != type) 
			{
				error.found++;
				error.errors.push( new message("found "+typeOf(param)+" Expected "+type+msg) );
			}
		},
		nameValid:function(expected,data,msg)
		{
			if(!data[expected])
			{ 
				error.found++;
				error.errors.push( new message(" property: '"+expected+"' not found "+msg));
			}
		}
	}
	var error ={ found:0,errors:[] }
	var emptyObject={ typeOf : function(){ return 'empty data';}}
	function typeOf(type)
	{
		if(!type) return 'null';
		if (type.typeOf) return type.typeOf;
		if (typeof type == 'string') return 'String';
		if (typeof type == 'number') return 'Number';
		if (typeof type == 'function') return 'Function';
		if (typeof type == 'object' && type instanceof Array) return 'Array';
		if (typeof type == 'object') return 'Object';
	}
	var message = function(msg){ this.msg = msg;}
	
	return JSV;
})();